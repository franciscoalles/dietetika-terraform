
resource "aws_vpc" "VPC1" {
  cidr_block = "172.16.0.0/16"
   enable_dns_hostnames = true

  tags = {
    Name = "VPC-PFI"
  }
}

 
resource "aws_subnet" "subnet1" {
  vpc_id            = aws_vpc.VPC1.id
  cidr_block        = "172.16.10.0/24"
  availability_zone = var.region
  map_public_ip_on_launch = true
  depends_on = [aws_internet_gateway.gw]
  tags = {
    Name = "subnet1-PFI"
  }
}

resource "aws_subnet" "subnet2" {
  vpc_id            = aws_vpc.VPC1.id
  cidr_block        = "172.16.15.0/24"
  availability_zone = var.region2
  map_public_ip_on_launch = true
  depends_on = [aws_internet_gateway.gw]
  tags = {
    Name = "subnet2-PFI"
  }
}


resource "aws_network_interface" "network_interface" {
  subnet_id   = aws_subnet.subnet1.id
  private_ips = ["172.16.10.100"]
    security_groups = [ aws_security_group.web-sg.id ]
  tags = {
    Name = "primary_network_interface"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.VPC1.id
}



resource "aws_security_group" "web-sg" {
  name = "web-sg"
  vpc_id = aws_vpc.VPC1.id
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

    ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

    ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

