data "aws_ami" "amazon_linux_2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-ebs"]
  }
}


resource "aws_launch_template" "template" {
  name_prefix   = "VM-App"
  image_id      = data.aws_ami.amazon_linux_2.id
  instance_type = "t2.micro"
  key_name = "terraform-key"
}

resource "aws_autoscaling_group" "bar" {
  name = "ASG"
  #availability_zones = [var.region]
  desired_capacity   = 1
  max_size           = 1
  min_size           = 1
  health_check_grace_period = 300
  health_check_type         = "EC2"
  force_delete              = true
  vpc_zone_identifier       = ["${aws_subnet.subnet1.id}", "${aws_subnet.subnet2.id}"]
  target_group_arns         = ["${aws_lb_target_group.lb-group.arn}"]
  launch_template {
    id      = aws_launch_template.template.id
    version = "$Latest"
  }
}


resource "aws_autoscaling_policy" "alb-policy" {
  name = "alb-policy"
  policy_type = "TargetTrackingScaling"
  estimated_instance_warmup = 200 
  autoscaling_group_name = aws_autoscaling_group.bar.name
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }

    target_value = 80.0
  }
} 



