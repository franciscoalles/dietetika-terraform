resource "aws_s3_bucket" "dietetika-bucket" {
  bucket = "dietetika-bucket"
  acl    = "private"

  tags = {
    Name        = "Bucket Dietetika App"
    Environment = "PRD"
  }
}