# PFI-Dietetika (Terraform)
Bienvenido al proyecto de Dietetika!
Este módulo representa a la receta de Terraform, la cual permite la creación de todos los servicios de infraestrcutrua en el tenant de AWS integrado.
---
REQUISITOS
-
Para lavantar el proyecto se debe cumplir con los siguientes requisitos:

- Instancia Amazon Web Services
- Terraform instalado
- Variables del sistema operativo configuradas
---
Ejecución del proyecto manual
-
1. Abrir consola de Terraform
2. Ubicarse sobre la ruta de archivos de receta
3. Ejecutar comando "terraform init" para levantar el directorio
4. Ejecutar comando "terraform validate" para validar que las configuraciones sean válidas
5. Ejecutar comando "terraform apply" para crear todos los servicios de AWS

---
Ejecución del proyecto automática
-
1. Ingresar sobre repositorio de  GitLab
2. Ingresar al proyecto de Terraform
3. Abrir pipelines
4. Ejecutar pipeline y aprobar el apply final
---

Nota: Modificar ACCESS KEY y SECRET KEY en el archivo "terraform.tfvars" para conectar con otra instancia de AWS.