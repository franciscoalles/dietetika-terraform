resource "aws_db_instance" "db-mysql" {
  publicly_accessible = false
  db_subnet_group_name = "${aws_db_subnet_group.db-subnet.name}"
  allocated_storage    = 50
  max_allocated_storage = 100
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  name                 = "mydb"
  identifier           = "mydb"
  username             = "admin"
  password             = "admin1234"
  parameter_group_name = "default.mysql5.7"
  skip_final_snapshot  = true
 #Descomentar el multi-az 
 #multi_az = true
  
  #backup_retention_period = 7
  apply_immediately = true
}


resource "aws_subnet" "rds1" {
  vpc_id                  = aws_vpc.VPC1.id
  cidr_block              = "172.16.11.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1a"
  
}

resource "aws_subnet" "rds2" {
  vpc_id                  = aws_vpc.VPC1.id
  cidr_block              = "172.16.12.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1b"
  
}


resource "aws_db_subnet_group" "db-subnet" {
name = "subnet-group"
subnet_ids = ["${aws_subnet.rds1.id}", "${aws_subnet.rds2.id}"]
}

resource "aws_security_group" "rds" {
  name        = "terraform_rds_security_group"
  description = "Terraform example RDS MySQL server"
  vpc_id      = aws_vpc.VPC1.id
  # Keep the instance private by only allowing traffic from the web server.
  ingress {
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # Allow all outbound traffic.
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
 
}



