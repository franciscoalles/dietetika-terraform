resource "aws_lb" "alb" {
  name               = "alb-tf"
  subnets = ["${aws_subnet.subnet1.id}", "${aws_subnet.subnet2.id}"]
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.web-sg.id]

  enable_deletion_protection = false
  
}


resource "aws_lb_target_group" "lb-group" {
  name     = "lb-group"
  depends_on  = [aws_vpc.VPC1]
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${aws_vpc.VPC1.id}"
  target_type = "instance"

    health_check {
    interval            = 30
    path                = "/index.html"
    port                = 80
    healthy_threshold   = 5
    unhealthy_threshold = 2
    timeout             = 5
    protocol            = "HTTP"
    matcher             = "200,202"
  }

}

resource "aws_lb_listener" "lb-listener" {

  load_balancer_arn = "${aws_lb.alb.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.lb-group.arn}"
    type             = "forward"
  }
}